/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package karaoke_manager;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 *
 * @author trant
 */
public class Karaoke_Manager extends JFrame{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Karaoke_Manager k=new Karaoke_Manager();
    }
    JFrame f1;
    JPanel p1;
    JLabel l1, l2, l3;
    JTextField t1;
    JPasswordField pass;
    JButton b1, b2;
    ImageIcon img;
    
    Karaoke_Manager() {

        p1 = new JPanel();
        p1.setLayout(null);
        p1.setBackground(new Color(135, 206, 250));

        img = new ImageIcon(getClass().getResource("header.png"));

        l1 = new JLabel(img);
        l1.setBounds(350, 0, 215, 200);

        l2 = new JLabel("Username");
        l2.setBounds(10, 30, 90, 40);
        l2.setFont(new Font("Serif", Font.PLAIN, 20));

        l3 = new JLabel("Password");
        l3.setBounds(10, 120, 100, 40);
        l3.setFont(new Font("Serif", Font.PLAIN, 20));

        t1 = new JTextField(30);
        t1.setBounds(100, 30, 230, 35);

        pass = new JPasswordField(10);
        pass.setBounds(100, 120, 230, 35);

        b1 = new JButton("User-Login");
        b1.setBounds(50, 250, 100, 50);
        b1.setBackground(new Color(204, 229, 255));

        b2 = new JButton("Administrator-Login");
        b2.setBounds(180, 250, 150, 50);
        b2.setBackground(new Color(204, 229, 255));

        p1.add(l1);
        p1.add(l2);
        p1.add(l3);
        p1.add(t1);
        p1.add(pass);
        p1.add(b1);
        p1.add(b2);

        add(p1);
        setSize(580, 390);
        setVisible(true);
        setResizable(false);
        setLocation(370, 50);
        setTitle("Karaoke_Manager");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        userlogin();
        adminlogin();

    }
    public void userlogin() {
        b1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String puname = t1.getText();
                String ppaswd = pass.getText();
                if (puname.equals("user") && ppaswd.equals("user")) {
                    NhanVien re = new NhanVien();
                    re.setVisible(true);
                    dispose();
                } else {

                    JOptionPane.showMessageDialog(null, "Wrong Password / Username");
                    t1.setText("");
                    pass.setText("");
                    t1.requestFocus();
                }
            }
        });
    }

    public void adminlogin() {
        b2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String puname = t1.getText();
                String ppaswd = pass.getText();
                if (puname.equals("admin") && ppaswd.equals("admin")) {
                   // AdminFrame re = new AdminFrame();
                    //re.setVisible(true);
                    dispose();
                } else {

                    JOptionPane.showMessageDialog(null, "Wrong Password / Username");
                    t1.setText("");
                    pass.setText("");
                    t1.requestFocus();
                }
            }
        });
    }
}
