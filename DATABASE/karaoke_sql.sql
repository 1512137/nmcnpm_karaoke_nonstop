﻿if DB_ID('karaoke_sql') is not null 
drop database karaoke_sql
go
create database karaoke_sql
go
use karaoke_sql
go

create table KS_ACCOUNT_USER
(
MaNV char(5),
Pass char(50),
primary key (MaNV)
)
go
create table KS_NHANVIEN
(
MaNV char(5),
HoTen nvarchar(30),
NgaySinh datetime,
DiaChi nvarchar(30),
NgayDiLam datetime, -- lấy cái này để làm cái tính lương
primary key (MaNV)
)
go
create table KS_MONAN
(
MaMA char(5),
TenMA nvarchar(30),
DonGia int,
primary key (MaMA)

)
go
create table KS_HOADON1
(
MaHD char(5),
MaPhong char(5),
ThoiGianBD time,
ThoiGianKT time,
NhanVienPhuTrach char(5),
primary key (MaHD)

)
go
create table KS_HOADON2
(
MaHD char(5),
STT int,
MaMA char(5),
SL int ,
primary key (MaHD,STT)
)

go
create table KS_PHONG
(
MaPhong char(5),
TenPhong nvarchar(30),
LoaiPhong int , -- <CHƯA CẦN ĐỂ Ý > giảm theo thứ tự loại phòng vip 1, vip 2, vip 3
primary key (MaPhong)
)
go
-- Các Khóa PHỤ
-- BẢNG KS_NHANVIEN
alter table KS_NHANVIEN add constraint FK_NV_AC foreign key (MaNV) references KS_ACCOUNT_USER(MaNV)
go
-- BẢNG KS_MONAN
	-- nothing
-- BẢNG HÓA ĐƠN 1
alter table KS_HOADON1 add constraint FK_DH1_NV foreign key (NhanVienPhuTrach) references KS_NHANVIEN(MaNV)
go
alter table KS_HOADON1 add constraint FK_DH1_P foreign key (MaPhong) references KS_PHONG(MaPhong)
go
-- Bảng HÓA ĐƠN 2
alter table KS_HOADON2 add constraint FK_DH2_HD1 foreign key (MaHD) references KS_HOADON1(MaHD)
go
-- Bảng PHÒNG
	-- nothing


-- INSERT ACCOUNT USER
insert into KS_ACCOUNT_USER(MaNV,Pass) values('NV001','12345')
go
insert into KS_ACCOUNT_USER(MaNV,Pass) values('NV002','123456')
go
insert into KS_ACCOUNT_USER(MaNV,Pass) values('NV003','1234567')
go


-- INSERT MÓN ĂN
insert into KS_MONAN(MaMA,TenMA,DonGia) values ('MA001',N'Dĩa Xoài',10000)
go
insert into KS_MONAN(MaMA,TenMA,DonGia) values ('MA002',N'Dĩa Ổi',10000)
go
insert into KS_MONAN(MaMA,TenMA,DonGia) values ('MA003',N'Dĩa Thập Cẩm',50000)
go
insert into KS_MONAN(MaMA,TenMA,DonGia) values ('MA004',N'Nước Mía',10000)
go
insert into KS_MONAN(MaMA,TenMA,DonGia) values ('MA005',N'Nước Cocacola',10000)
go
insert into KS_MONAN(MaMA,TenMA,DonGia) values ('MA006',N'Milk',10000)
go
insert into KS_MONAN(MaMA,TenMA,DonGia) values ('MA007',N'Xô Đá',10000)
go
insert into KS_MONAN(MaMA,TenMA,DonGia) values ('MA008',N'Dĩa Muối Tôm Rang',10000)
go
insert into KS_MONAN(MaMA,TenMA,DonGia) values ('MA009',N'Dĩa Cóc ngâm',10000)
go
insert into KS_MONAN(MaMA,TenMA,DonGia) values ('MA010',N'Osi',10000)
go

-- INSERT NHÂN VIÊN
insert into KS_NHANVIEN(MaNV,HoTen,NgaySinh,DiaChi,NgayDiLam) values
('NV001',N'Nguyễn Văn A','4/13/1997',N'Tp. Hồ Chí Minh','11/10/2017')
go
insert into KS_NHANVIEN(MaNV,HoTen,NgaySinh,DiaChi,NgayDiLam) values
('NV002',N'Nguyễn Văn B','1/13/1997',N'Tp. Hồ Chí Minh','11/10/2017')
go
insert into KS_NHANVIEN(MaNV,HoTen,NgaySinh,DiaChi,NgayDiLam) values
('NV003',N'Nguyễn Văn C','7/26/1998',N'Tp. Hồ Chí Minh','11/11/2017')
go



--INSERT PHÒNG

insert into KS_PHONG(MaPhong,TenPhong,LoaiPhong) values ('MP001',N'Phòng California',1)
go
insert into KS_PHONG(MaPhong,TenPhong,LoaiPhong) values ('MP002',N'Phòng Siêu Nhân',2)
go

--INSERT HÓA ĐƠN 1
insert into KS_HOADON1(MaHD,MaPhong,ThoiGianBD,ThoiGianKT,NhanVienPhuTrach) values ('HD001','MP001','7:00am','9:00am','NV002')
go
insert into KS_HOADON1(MaHD,MaPhong,ThoiGianBD,ThoiGianKT,NhanVienPhuTrach) values ('HD002','MP001','7:00am','1:00pm','NV002')
go
insert into KS_HOADON1(MaHD,MaPhong,ThoiGianBD,ThoiGianKT,NhanVienPhuTrach) values ('HD003','MP002','7:00am','9:00pm','NV001')
go

--INSERT HÓA ĐƠN 2

insert into KS_HOADON2(MaHD,STT,MaMA,SL) values ('HD001',1,'MA001',10)
go
insert into KS_HOADON2(MaHD,STT,MaMA,SL) values ('HD001',2,'MA005',5)
go

